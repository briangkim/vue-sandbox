export function createTickerRow(ticker, data) {
  let tickerRow = {
    type: 'TICKER',
    id: createUniqueId(ticker),
    ticker: ticker,
    data: Object.assign({}, data),
  };
  return tickerRow;
}

export function createHeaderRow(label) {
  let headerRow = {
    type: 'HEADER',
    id: createUniqueId(label),
    label: label,
    isCollapsed: false,
  };
  return headerRow;
}

function createUniqueId(str) {
  let date = new Date();
  let timeMs = date.getTime();
  return str + ':' + timeMs;
}
