export function addTodo(payload) {
  return {type: 'ADD_TODO', payload}
};

export function toggleTodo(payload) {
  return {type: 'TOGGLE_TODO', payload}
};