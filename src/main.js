import Vue from 'vue';

import Todo from './components/Todo';
import WordGame from './word-game/WordGame';

//import prefixes from './words/prefixes';
//import suffixes from './words/suffixes';
//
//_.each(prefixes, prefix => {
//  _.each(suffixes, suffix => {
//    console.log(`${prefix}${suffix}`);
//  });
//});

new Vue({
  el: '#app',
  template: '<WordGame/>',
  components: { WordGame },
});
